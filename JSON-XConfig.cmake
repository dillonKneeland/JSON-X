get_filename_component(JSON-X_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)

if(NOT TARGET JSON-X::JSON-X)
	include("${JSON-X_CMAKE_DIR}/JSON-XTargets.cmake")
endif()