/*
 * JSON.hpp
 *
 * Author: Dillon Kneeland
 */

#ifndef JSON_HPP_
#define JSON_HPP_

#include <memory>
#include <string>
#include <queue>
#include <map>
#include <stdexcept>

namespace json
{
enum ValueType
{
	NONE, OBJECT, STRING, FLOAT, ARRAY
};
typedef std::basic_string<wchar_t> String;
typedef long double Float;

class JNode
{
private:
public:
	JNode() {}
	virtual ~JNode() {};

	virtual JNode& operator[](String) = 0;
	virtual JNode& operator[](int) = 0;
	virtual String GetString() = 0;
	virtual Float GetFloat() = 0;
	virtual ValueType GetType() = 0;
};

class JObject : public JNode, public std::map<String, std::shared_ptr<JNode>>
{
public:
	virtual JNode& operator[](String key) override
	{ return *std::map<String, std::shared_ptr<JNode>>::operator[](key); }
	virtual JNode& operator[](int) override
	{ throw std::logic_error("Node is not an array."); }
	virtual String GetString() override
	{ throw std::logic_error("Node is not a string value."); }
	virtual Float GetFloat() override
	{ throw std::logic_error("Node is not a float value."); }
	virtual ValueType GetType() override
	{ return ValueType::OBJECT; }
};

class JArray : public JNode, public std::queue<JNode> {
public:
	virtual JNode& operator[](String) override
	{ throw std::logic_error("Node is not an object."); }
	virtual JNode& operator[](int index) override
	{ return std::queue<JNode>::operator[](index); }
	virtual String GetString() override
	{ throw std::logic_error("Node is not a string value."); }
	virtual Float GetFloat() override
	{ throw std::logic_error("Node is not a float value."); }
	virtual ValueType GetType() override
	{ return ValueType::ARRAY; }
};

class JString : public JNode
{
private:
	String _value;
public:
	explicit JString(String const& value) : _value(value) {}
	virtual JNode& operator[](String) override
	{ throw std::logic_error("Node is not an object."); }
	virtual JNode& operator[](int index) override
	{ throw std::logic_error("Node is not an object."); }
	virtual String GetString() override
	{ return _value; }
	virtual Float GetFloat() override
	{ throw std::logic_error("Node is not a float value."); }
	virtual ValueType GetType() override
	{ return ValueType::STRING; }
};

class JFloat : public JNode
{
private:
	Float _value;
public:
	explicit JFloat(Float const& value) : _value(value) {}
	virtual JNode& operator[](String) override
	{ throw std::logic_error("Node is not an object."); }
	virtual JNode& operator[](int index) override
	{ throw std::logic_error("Node is not an object."); }
	virtual String GetString() override
	{ throw std::logic_error("Node is not a string value."); }
	virtual Float GetFloat() override
	{ return _value; }
	virtual ValueType GetType() override
	{ return ValueType::FLOAT; }
};
}

#endif /* JSON_HPP_ */
